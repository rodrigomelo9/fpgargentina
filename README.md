# FPGArgentina

Resultados del relevamiento disponibles [AQUÍ](https://rodrigomelo9.gitlab.io/fpgargentina/).

Relevamiento de grupos de desarrollo, capacidades y casos de uso de la tecnología FPGA en Argentina.
Distribuido bajo licencia *Creative Commons Attribution 4.0 International*
[(CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/).

> **Descargo de responsabilidad:**
> este listado se distribuye con la intención de que resulte útil, pero sin ninguna garantía de exactitud o estado actualizado.
> Es principalmente resultado de la información recolectada, inicialmente mediante un formulario, y enriquecida con el tiempo.
> Si detectas información desactualizada, faltante o incorrecta, no dudes en crear un
> [issue](https://gitlab.com/rodrigomelo9/fpgargentina/-/issues) o directamente modificarlo y solicitar un
> [merge request](https://gitlab.com/rodrigomelo9/fpgargentina/-/merge_requests).
> Si sos responsable del grupo, y no deseas que el mismo figure en el listado, contactate para que lo removamos.
