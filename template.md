---
# Los metadatos se completan en formato YAML
# Si alguno no se completa, debe ser directamente obviado.
# <ALGO> debe ser remplazado por uno o más valores correspondientes
# <CATEGORIA> = empresa | universidad | organismo | independiente
# En proyectos o casos de uso, indicar los que tienen relación con FPGA
# La oferta de capacitación, es relacionada a cursos que se brinden sobre la temática.
denominacion: "<DENOMINACION>"
categoria: "<CATEGORIA>"
asic: True # Solo si se dedica a ASIC en lugar de FPGAs
provincias: ["<PROVINCIA1>", "<PROVINCIA2>"]
localidades: ["<LOCALIDAD1>", "<LOCALIDAD2>"]
sitio: "<URL>"
fabricantes: [Xilinx, Intel, Microchip, Lattice, <OTRA>]
dispositivos: [<LISTADO DE DISPOSITIVOS Y/O PLACAS>]
lenguajes: [VHDL, Verilog, "System Verilog", "Xilinx HLS", <ETC>]
herramientas: [Vivado, ISE, Quartus, Libero-SoC, <ETC>]
opensource: [<LISTADO DE HERRAMIENTAS OPEN SOURCE>]
actualizado: "<DD/MM/AÑO>"
---

## Proyectos, productos o casos de uso

* <PROYECTO 1>
* <PROYECTO 2>
* <PROYECTO 3>

## Ofertas de capacitación

* <OFERTA 1>
* <OFERTA 2>
* <OFERTA 3>
