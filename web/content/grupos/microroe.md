---
denominacion: "Microroe Ingeniería Electrónica SRL"
categoria: "empresa"
sitio: "https://www.microroe.com/"
provincias: ["Buenos Aires"]
localidades: ["La Plata"]
fabricantes: ["Xilinx"]
dispositivos: ["Spartan-3", "Spartan-6", "placas custom"]
lenguajes: [VHDL, Verilog]
herramientas: [ISE]
actualizado: "16-11-2020"
---

## Proyectos, productos o casos de uso

* Sistemas de comunicaciones con herencia de vuelo, Radar, Sistema de Navegacion.
* Aplicaciones con procesadores embebidos.
