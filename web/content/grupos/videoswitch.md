---
denominacion: "VideoSwitch"
categoria: "empresa"
sitio: "http://www.videoswitch.tv/"
provincias: ["CABA"]
localidades: ["CABA"]
fabricantes: ["Intel"]
dispositivos: ["Cyclone-III", "Cyclone-V", "Placas Custom"]
lenguajes: [VHDL, Verilog]
herramientas: [Quartus, Modelsim]
actualizado: "17-11-2020"
---

## Proyectos, productos o casos de uso

* Productos para procesamiento de señales de video y television digital.
