---
denominacion: "Boherdi Electrónica"
categoria: "empresa"
sitio: "http://www.boherdi.com/productos/sistema-pai/"
provincias: ["CABA"]
localidades: ["CABA"]
fabricantes: ["Intel"]
dispositivos: ["Cyclone-II", "placas custom"]
lenguajes: [VHDL]
herramientas: [Quartus]
actualizado: "16-11-2020"
---

## Proyectos, productos o casos de uso

* PAI (protección de arco)
