---
denominacion: "UV-VIS Metrolab S.A."
categoria: "empresa"
provincias: ["CABA"]
localidades: ["CABA"]
fabricantes: ["Intel"]
dispositivos: ["Cyclone-II"]
herramientas: [Quartus]
actualizado: "17-11-2020"
---

## Proyectos, productos o casos de uso

* Control de señales para fotómetro de alta velocidad.
* Bus LVDS hacia ADC con 250MHz de clock de datos.
