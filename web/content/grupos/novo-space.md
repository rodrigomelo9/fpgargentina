---
denominacion: "Novo Space"
categoria: "empresa"
sitio: "https://www.novo.space"
provincias: ["CABA"]
localidades: ["CABA"]
fabricantes: ["Xilinx"]
dispositivos: ["Zynq-7000", "Zynq-UltraScale+", "placas custom"]
lenguajes: [VHDL]
herramientas: [Vivado, Libero-SoC, Modelsim]
actualizado: "16-11-2020"
---

## Proyectos, productos o casos de uso

* Placas digitales de alto desempeño para la industria espacial (CPUs, DSPs, memoria masivas, etc).
