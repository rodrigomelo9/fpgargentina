---
denominacion: "INVAP S.E."
categoria: "empresa"
sitio: "http://www.invap.com.ar/es/espacial-y-gobierno/area-aeroespacial-y-gobierno/introduccion.html"
provincias: ["Río Negro", "Córdoba"]
localidades: ["Bariloche", "Córdoba"]
fabricantes: [ "Xilinx", "Microchip"]
dispositivos: [
  "Xilinx series anteriores", "Xilinx Serie 6", "Xilinx Serie 7", "Zynq-7000",
  "Xilinx MPSoC", UltraScale, "UltraScale+", RTAX, RTG4
]
lenguajes: [VHDL, Verilog]
herramientas: [ISE, Vivado, "Libero-SoC"]
actualizado: "17-11-2020"
---

## Proyectos, productos o casos de uso

* Satelital: Saocom, Sabiamar, SmallGeo, Arsat.
* Radares: RPA, RSMA, RMA, RASIT, TPS, PSR.
* Aplicaciones para generar equipos de prueba para estos desarrollos.
