---
denominacion: "Fuerza Aerea Argentina (FAA) - Centro de Investigaciones Aplicadas (CIA)"
categoria: "organismo"
provincias: ["Córdoba"]
localidades: ["Córdoba"]
fabricantes: [Xilinx, Microchip]
dispositivos: [SmartFusion2, "Eclypse Z7"]
lenguajes: [Verilog]
herramientas: [Vivado]
actualizado: "10-11-2020"
---

## Proyectos, productos o casos de uso

* Sistema de adquisicion de datos de radar, procesador monopulso, controlador de propulsor de plasma para microsatelites.
