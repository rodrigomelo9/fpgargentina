---
denominacion: "Universidad Nacional de Río Cuarto (UNRC)- Laboratorio de Sistemas Embebidos (LaSEm)"
categoria: "universidad"
sitio: "https://www.ing.unrc.edu.ar/laboratorios.php"
provincias: ["Córdoba"]
localidades: ["Río Cuarto"]
fabricantes: [Xilinx, Intel]
dispositivos: ["Basys 3", Arty, "Spartan-3E"]
lenguajes: [VHDL, Verilog]
herramientas: [Vivado, Quartus]
actualizado: "18-11-2020"
---

## Proyectos, productos o casos de uso

* Proyectos de alumnos publicados en congresos. 
* Osciloscopio virtuales y procesadores con fines didácticos.

## Ofertas de capacitación

* Cursos de postgrado en Universidades Nacionales 
* Servicio de capacitación desde CONICET
