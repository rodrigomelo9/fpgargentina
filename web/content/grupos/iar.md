---
denominacion: "Instituto Argentino de Radioastronomia (IAR)"
categoria: "organismo"
provincias: ["Buenos Aires"]
localidades: ["La Plata", "Berazategui"]
sitio: "https://www.iar.unlp.edu.ar/"
fabricantes: [Xilinx]
dispositivos: ["Spartan-3E", "Virtex-5", "UltraScale+", "Qpro-R Virtex"]
lenguajes: [VHDL, Verilog]
herramientas: [Vivado, ISE, Matlab, Simulink]
actualizado: "14-12-2020"
---

> Depende de:
> * Consejo Nacional de Investigaciones Científicas y Técnicas (CONICET)
> * Comisión de Investigaciones Científicas de la Provincia de Buenos Aires (CICPBA)
> * Universidad Nacional de La Plata (UNLP)

## Proyectos, productos o casos de uso

* CASPER-ROACH, (Reconfigurable Open Architecture Computing Hardware) standalone FPGA processing board.
* Adquisición de datos de los radiotelescopios del IAR.
* Event Horizon Telescope.
* Thermal InfraRed Camera, misión SABIAMar.
