---
denominacion: "Comisión Nacional de Energía Atómica (CNEA) - Instrumentación y Control"
categoria: "organismo"
sitio: "http://www.argentina.gob.ar/metrologia-y-detectores/instrumentacion-y-control"
provincias: ["Buenos Aires"]
localidades: ["Ezeiza"]
fabricantes: [ "Xilinx", "Microchip"]
dispositivos: [
  Spartan-3, Spartan-6, Virtex-4, Zynq-7000, Microzed, Zedboard, Artix-7, Arty, Proasic3
]
lenguajes: []
herramientas: [ISE, Vivado, Libero-SoC]
actualizado: "10-11-2020"
---

## Proyectos, productos o casos de uso

* Equipos de medicina nuclear (Tomógrafo por emisión de positrones AR-PET)
* Equipos para la prosprección geológica, caracterización de residuos radiactivos, radioprotección y espectrometría gama. 
* Instrumentación para Reactores de Investigación y Centrales Nucleares.
