---
denominacion: "I-Tera"
categoria: "empresa"
asic: True
sitio: "http://www.i-terasic.com"
provincias: ["Córdoba"]
localidades: ["Córdoba"]
fabricantes: [Xilinx]
dispositivos: [Artix-7, Nexys]
lenguajes: [Verilog]
herramientas: [Vivado]
actualizado: "16-11-2020"
---

> Es empresa de desarrollo de ASIC.

## Proyectos, productos o casos de uso

* Utilizan FPGA para prototipado y pruebas de concepto.
* Experiencia con softcores LEON-3 y Microblaze.
