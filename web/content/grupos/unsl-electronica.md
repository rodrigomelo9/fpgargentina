---
denominacion: "Universidad Nacional de San Luis (UNSL) - Facultad de Ciencias Físico, Matemáticas y Naturales (FCMyN) - Departamento de Electronica"
categoria: "universidad"
sitio: "http://fmn.unsl.edu.ar/"
provincias: ["San Luis"]
localidades: ["San Luis"]
fabricantes: [Xilinx, Intel, Microchip]
dispositivos: [
  "Zynq-7000", Zedboard, Zybo,
  Virtex, Nexys-3, Nexys-4, Artix-7, UltraScale,
  SmartFusion, "DE0 Nano", DE1
]
lenguajes: [VHDL, "Xilinx HLS", SystemC]
herramientas: [ISE, Vivado, "Vivado HLS", Quartus, Libero-SoC, Modelsim, "System Generator"]
actualizado: "30-11-2020"
---

## Proyectos, productos o casos de uso

* Inversor trifásico utilizando las transformadas Clark, Park y Vivado HLS.
* Módulo i2c implementado en VHDL.
* Control de velocidad de un motor trifásico tensión/frecuencia usando Vivado HLS.
* Diseño de filtros digitales FIR e IIR para sitemas embebidos en SoC reconfigurables y programables.
* Uso de diferentes técnicas de descripción de hardware: Técnicas de síntesis de alto nivel con Vivado HLS, síntesis por traducción de bloques de modelado con System Generator, uso de generadores de core parametrizados como Core Gen, descripción directa en HDL, etc.
* Diseño de generadores de señales.
* Diseño de instrumentos de análisis de señales.
* Reconfiguración Parcial Dinámica (RPD), Schedulers para RPD, dynDet tool para RPD, Grid de FPGAs, Instrumentacion científica usando FPGA, Instrumentación virtual, Procesamiento de señales en tiempo real en general y simulación de Montecarlo en FPGA.
* Tremor reduction, dispositivo para la reducción del temblor esencial basada en MPSoCs.
* Accurate Depth Estimation, and object recognition, Estimación de profundidad y reconocimientos de objetos en tiempo real usando FPGAs.
* Seismic Full Waveform Invertion, método de generación de imágenes sísmicas que permite recuperar parámetros del subsuelo muy detallados, utilizando toda la información contenida en las lecturas de los sensores sísmicos sin procesar.

## Ofertas de capacitación

* Procesamiento digital de señales con FPGA y SoC.
* Síntesis por traducción de bloques de modelado con System Generator y uso de generadores de core parametrizados como Core Gen.
* Maestría en Sistemas Embebidos donde hay contenidos sobre FPGA.
