---
denominacion: "Instituto Nacional de Tecnología Industrial (INTI) - Departamento de Comunicaciones"
categoria: "organismo"
sitio: "http://www.inti.gob.ar/areas/servicios-industriales/electr%C3%B3nica-y-energia/electronica-y-tics"
provincias: ["Buenos Aires"]
localidades: ["San Martín"]
fabricantes: [Xilinx, Intel, Microchip]
dispositivos: [Virtex-5, Virtex-6, ML605, Zynq-7000, CIAA-ACC]
lenguajes: [VHDL, Verilog]
herramientas: [ISE, Vivado, Quartus, Modelsim]
opensource: [cocotb, GHDL, iVerilog, GTKwave, HDLmake]
actualizado: "10-11-2020"
---

## Proyectos, productos o casos de uso

* Plataforma de desarrollo para sensado y comunicaciones Ultra-Wideband, Modulador de TV digital ISDB-T, Ecualizador Adaptativo LMS para estimación del canal.

## Ofertas de capacitación

* Procesamiento Digital de Señales sobre FPGA.
