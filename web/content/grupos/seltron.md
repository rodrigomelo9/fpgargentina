---
denominacion: "Seltron"
categoria: "empresa"
sitio: "http://www.seltron.com.ar/servicios_L%C3%B3gica_Programable_FPGA.html"
provincias: ["Córdoba"]
localidades: ["Córdoba"]
fabricantes: [Xilinx, Microchip]
dispositivos: [AT40K, "Placas Custom"]
herramientas: [ISE, Libero-SoC, Questa, Modelsim, "Gaisler Lib"]
actualizado: "17-11-2020"
---

## Proyectos, productos o casos de uso

* Sasori CPL, SBC y IAB Sabia-Mar, Telemetría Tronador II.
