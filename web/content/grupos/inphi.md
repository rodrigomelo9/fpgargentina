---
denominacion: "Inphi Argentina"
categoria: "empresa"
asic: True
sitio: "https://www.inphi.com/"
provincias: ["Córdoba"]
localidades: ["Córdoba"]
actualizado: "16-11-2020"
---

> Es empresa de desarrollo de ASIC. No proporcionó información oficial, pero poseen placas FPGA, probablemente para prototipado y pruebas de concepto.
