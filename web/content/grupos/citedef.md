---
denominacion: "Instituto de Investigaciones Científicas y Técnicas para la Defensa (CITEDEF) - Laboratorio de Técnicas Digitales"
categoria: "organismo"
provincias: ["Buenos Aires"]
localidades: ["Villa Martelli"]
fabricantes: [ "Xilinx"]
dispositivos: ["Emtech 3PX1", "Nexys 3"]
lenguajes: [VHDL]
herramientas: [ISE]
actualizado: "10-11-2020"
---

## Proyectos, productos o casos de uso

* Sub-sistema de instrumentación de datos, deconmutación de mensajes y control de actuadores orientado a UAVs

## Ofertas de capacitación

* Curso introductorio a Lógica Programable.
