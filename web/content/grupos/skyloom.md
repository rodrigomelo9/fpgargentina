---
denominacion: "Skyloom"
categoria: "empresa"
sitio: "https://www.skyloom.co/"
provincias: ["CABA"]
localidades: ["CABA"]
fabricantes: [Xilinx, Microchip]
dispositivos: ["Spartan-6", "Kintex Ultrascale+"]
lenguajes: [VHDL]
herramientas: [ISE, Vivado]
actualizado: "17-11-2020"
---

## Proyectos, productos o casos de uso

* Optical Comms Codig & Sync, Forward Error Correction.
