---
denominacion: "Universidad Tecnológica Nacional (UTN) - Facultad Regional Haedo (FRH) - Grupo de Aplicaciones de Sistemas Embebidos (ASE)"
categoria: "universidad"
sitio: "http://www.frh.utn.edu.ar/secretaria_academica/investigacion_posgrado"
provincias: ["Buenos Aires"]
localidades: ["Haedo"]
fabricantes: [Xilinx, Intel, Lattice]
dispositivos: [Spartan-3, Arty, Zybo, Kefir, ICEstick, EDU-CIAA-FPGA, DE2-115]
lenguajes: [VHDL, Verilog, "Xilinx HLS"]
herramientas: [Vivado, ISE, "Vivado HLS", Vitis, ICECube2]
opensource: [ghdl, iverilog, gtkwave, yosys, arachne-pnr, next-pnr, icestorm]
actualizado: "21-11-2020"
---

## Proyectos, productos o casos de uso

* Desarrollo de receptor GPS basado en FPGA
* EDU-CIAA-FPGA: desarrollo de hardware, repositorio de proyectos y ejemplos, videos y material didáctico

## Ofertas de capacitación

* Workshops sobre temas específicos en el marco del Simposio Argentino de Sistemas Embebidos
