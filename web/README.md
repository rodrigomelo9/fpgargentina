# Fuentes del sitio web de FPGArgentina

La web de FPGArgentina es generada con [HUGO](https://gohugo.io).

Se construye automáticamente mediante [GitLab CI](../gitlab-ci.yml) y
desplegada en [GitLab pages](https://rodrigomelo9.gitlab.io/fpgargentina).

## Generación en ambiente local

### Instalar Hugo

Hay varios métodos para
[Instalar Hugo](https://gohugo.io/getting-started/installing/).
Uno es:
* [Instalar go](https://golang.org/doc/install)
* Agregar `export PATH=$PATH:/usr/local/go/bin:$HOME/go/bin` `.bashrc`
* `git clone https://github.com/gohugoio/hugo.git`
* `cd hugo`
* `go install --tags extended`

### Generación del sitio

* Para descargar submódulos Git (temas): `git submodule update --init --recursive`
* Entrar a directorio `web`.
* Para generar el sitio (queda en directorio `public`): `hugo`
* Para probar resultado de cambios en servidor local:
  * Ejecutar: `hugo server`
  * Con el navegador acceder a: `http://localhost:1313/fpgargentina`.
  * `Ctrl+C` para terminar la ejecución del servidor.
  * **Nota:** al cambiar un archivo del sitio, se regenera y recarga automáticamente.
