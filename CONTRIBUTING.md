# Contribuciones

Las formas en las que se puede contribuir al proyecto son varias:
* Indicando un grupo que conozcas y no este en el sitio, para que nos contactemos (queremos incluir a todos!).
* Informando sobre un error, omisión o estado desactualizado (buscamos brindar info fidedigna y actualizada).
* Dando aviso de un grupo que ya no trabaja con FPGAs, para verificarlo y removerlo.
* Realizando vos mismo una corrección o agregado de grupo, y solicitando un merge request.
  * El archivo que representa al grupo va en `web/content/grupos/<ID_DE_GRUPO>.md`.
  * Se puede utilizar [template.md](template.md) como punto de partida (posee indicaciones sobre cada item a completar).
  * También puede utilizarse el resto de archivos en `web/content/grupos/` ante dudas o como ejemplos.

Podes abrir un [issue](https://gitlab.com/rodrigomelo9/fpgargentina/-/issues) o realizar los cambios y solicitar un merge request.
Alternativamente, podes contactarnos por Linkedin:
* [Rodrigo Alejandro Melo](https://www.linkedin.com/in/rodrigoalejandromelo/)
* [Facundo Larosa](https://www.linkedin.com/in/facundo-santiago-larosa-200526a/)

## Descripción de campos a incluir en un grupo

* **Denominación:** nombre completo para identificar el grupo dentro de su organización.
* **Provincia**: una o mas provincias donde tengan sedes trabajando con FPGAs.
* **Localidad:** una o mas localidades donde tengan sedes trabajando con FPGAs.
* **URL:** idealmente que apunten a la actividad, pero sino puede ser el general de la empresa/institución.
* **Fabricantes:** lista de fabricantes que utilizan (ej: Xilinx, Intel, Microchip, Lattice).
* **Dispositivos/placas:** listado de placas y/o dispositivos que poseen (aunque sea algunos, los mas significativos, en concordancia con los fabricantes especificados).
* **Herramientas de desarrollo:** listado de herramientas con los que tienen experiencia (ej: Vivado, ISE, Quartus, Libero-SoC, Modelsim, Synplify, IceCube2, etc).
* **Lenguajes:** listado de lenguajes de desarrollo que utilizan (ej: VHDL, Verilog, System Verilog, Xilinx HLS, OpenCL, nMigen, etc).
* **Proyectos, Productos o casos de uso:** proyectos o casos de uso específicos donde utilizan FPGAs, aunque sea tematica relacionada.
* **Oferta de capacitación:** si brindan algún tipo de curso relacionado a la temática.
* **Herramientas open-source:** si es que utilizan, listado de herramientas open-source, especificas para el desarrollo con FPGAs/ASICs (ej: GHDL, iVerilog, Verilator, GTKwave, cocotb, Vunit, Yosys, RISC-V, etc)

**Notas:**
* URL puede ser obviada en caso de no tener sitio web.
* Fabricantes, dispositivos/placas, herramientas de desarrollo y lenguajes, solo deberían obviarse si no pueden ser indicados por temas de confidencialidad, pero son la base principal del relevamiento.
* Los proyectos y casos de uso, son una indicación deseable, para saber a que se dedica el grupo, cual es su campo de experticia.
* Oferta de capacitación y herramientas libres, son campos opcionales, solo en caso de corresponder.
